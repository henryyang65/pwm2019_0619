/*
 * clsSlotsOnOff.cpp
 *
 *  Created on: Jul. 2, 2019
 *      Author: yangh
 */

#include <clsSlotsOnOff.h>
#include "stm32f0xx_hal.h"


clsSlotsOnOff::clsSlotsOnOff(uint32_t m__slotNumber, uint32_t m__slot_ms)
	:
	mLast_tick(0),
	mSlot_ms_cnt(0),
	mSlot_x2(0),
	SlotNumber(m__slotNumber),
	Slot_ms(m__slot_ms)
{
}

clsSlotsOnOff::~clsSlotsOnOff() {
	// TODO Auto-generated destructor stub
}

uint32_t clsSlotsOnOff::slot(void)
{
	uint32_t tk=HAL_GetTick();
	if(tk!=mLast_tick)
	{
		mLast_tick=tk;
		mSlot_ms_cnt++;
		if(mSlot_ms_cnt>=Slot_ms)
		{
			mSlot_ms_cnt=0;
			mSlot_x2++;
			if(mSlot_x2>=2*SlotNumber)
			{
				mSlot_x2=0;
			}
		}
	}
	return mSlot_x2;
}
