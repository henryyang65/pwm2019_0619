/*
 * clsUartDma.cpp
 *
 *  Created on: Jul 1, 2019
 *      Author: yangh
 */

#include "clsUartDma.h"

#include <cstring>

clsUartDma  clsUartDma::uartClassObjects[uartClassObjectCount];


clsUartDma::clsUartDma()
	:
	 mpUART_HandleTypeDef( nullptr),
	 mReadIndex(0),
	 //mDmaIndex(0),
	 mReceByteCnt(0),
	 mLastDmaCycle(0),
	 mLastCNDTR(0),
	 mStatus(enumStatus::enumURDS_none),
	 mHAL_StatusTypeDef(HAL_StatusTypeDef::HAL_OK),
	 mDmaCycle(0),
	 mReadCycle(0),
	 mErrorCnt(0),
	 mByteCntTransmit(0),
	 mByteCntReceive(0),
	 mByteCntGet(0)
{


}


clsUartDma::~clsUartDma() {
	mpUART_HandleTypeDef=nullptr;
	mStatus = enumStatus::enumURDS_none;
}

bool clsUartDma::init(UART_HandleTypeDef* m__pUART_HandleTypeDef)
{
	if(mStatus != enumStatus::enumURDS_none)
		return false;
	if(m__pUART_HandleTypeDef==nullptr)
		return false;
	mpUART_HandleTypeDef=m__pUART_HandleTypeDef;
	mStatus = enumStatus::enumURDS_Idle;
	return true;
}

bool clsUartDma::StartReceive(void)
{
	if(mStatus == enumStatus::enumURDS_Idle)
	{
		mInitBuffer();
		mHAL_StatusTypeDef= HAL_UART_Receive_DMA(mpUART_HandleTypeDef, mBuffer, sizeof(mBuffer));
		if(mHAL_StatusTypeDef != HAL_StatusTypeDef::HAL_OK)
		{
			mStatus =  enumStatus::enumURDS_error;
			return false;
		}
		return true;
	}
	return false;
}

uint32_t clsUartDma::availableByteCount(void)
{
	uint32_t cndtr=mpUART_HandleTypeDef->hdmarx->Instance->CNDTR;
	uint32_t change=mLastCNDTR-cndtr;

	if(cndtr>mLastCNDTR)
	{
		change = (ReceingBUFFER_SIZE-(cndtr-mLastCNDTR));
	}
	mReceByteCnt+=change;
	mByteCntReceive+=change;
	mLastCNDTR=cndtr;

	return mReceByteCnt;

//	mDmaIndex=ReceingBUFFER_SIZE - mpUART_HandleTypeDef->hdmarx->Instance->CNDTR;
//	if(mDmaIndex>=mReadIndex)
//	{
//		return mDmaIndex-mReadIndex;
//	}
//	return mDmaIndex + ReceingBUFFER_SIZE-mReadIndex;
}

uint8_t clsUartDma::getByte(void)
{
	uint8_t b=mBuffer[mReadIndex++];
	if(mReadIndex>=ReceingBUFFER_SIZE)
	{
		mReadIndex=0;
		mReadCycle++;
	}
	mReceByteCnt--;
	mByteCntGet++;
	return b;
}

int clsUartDma::getByteCheck(void)
{
	if(availableByteCount()==0)
		return -1;

	uint8_t b=mBuffer[mReadIndex++];
	if(mReadIndex>=ReceingBUFFER_SIZE)
	{
		mReadIndex=0;
		mReadCycle++;
	}
	mReceByteCnt--;
	mByteCntGet++;
	return b;
}

HAL_StatusTypeDef clsUartDma::transmit(const uint8_t* m__pa, int m__size)
{
	if(m__pa==nullptr)
		return HAL_StatusTypeDef::HAL_ERROR;

	mWaitUartDmaFree();
	memcpy(mTransmitBuffer,m__pa,m__size);
	HAL_StatusTypeDef sts= HAL_UART_Transmit_DMA(mpUART_HandleTypeDef,mTransmitBuffer,m__size);
	if(sts==HAL_StatusTypeDef::HAL_OK)
		mByteCntTransmit+=m__size;
	return sts;
}

void clsUartDma::sInit(void)
{
}

void clsUartDma::dmaCycleCallBack(UART_HandleTypeDef* m__pUart)
{
	for(int i=0;i<uartClassObjectCount;i++)
	{
		clsUartDma& obj=uartClassObjects[i];
		if(obj.status()== enumStatus::enumURDS_none)
			return;
		obj.incDmaCycle(m__pUart);
	}
}

bool clsUartDma::mInitBuffer(void)
{
	if(mStatus == enumStatus::enumURDS_Receiving)
			return false;
	for(int i=0;i<ReceingBUFFER_SIZE;i++)
	{
		mBuffer[i]=0;
	}
	return true;
}
HAL_StatusTypeDef clsUartDma::transmit(const string & m__info)
{
//	for(char c : m__info)
//	{
//		while(true)
//		{
//			if( (mpUART_HandleTypeDef->Instance->ISR&0xc0) ==0xc0)
//			{
//				mpUART_HandleTypeDef->Instance->TDR=c;
//				break;
//			}
//		}
//	}

	//HAL_UART_Transmit(mpUART_HandleTypeDef,(uint8_t *)m__info.c_str(),m__info.size(),1);

	//HAL_UART_Transmit_DMA(mpUART_HandleTypeDef,(uint8_t *)m__info.c_str(),m__info.size());

	//while(mpUART_HandleTypeDef->hdmatx->State == HAL_DMA_StateTypeDef::HAL_DMA_STATE_BUSY){;}
	mWaitUartDmaFree();

	size_t len=m__info.size();
	len=(len>=Transmit_BUFFER_SIZE)?Transmit_BUFFER_SIZE:len;
	m__info.copy((char *)mTransmitBuffer, len, 0);
	while(true)
	{
		HAL_StatusTypeDef result=HAL_UART_Transmit_DMA(mpUART_HandleTypeDef,mTransmitBuffer,len);
		if(result==HAL_StatusTypeDef::HAL_BUSY)
			continue;
		if(result==HAL_StatusTypeDef::HAL_OK)
			mByteCntTransmit+=len;
		return result;
	}
}

HAL_StatusTypeDef clsUartDma::transmitDirect(const string & m__info)
{

	size_t len=m__info.size();
	len=(len>=Transmit_BUFFER_SIZE)?Transmit_BUFFER_SIZE:len;
	m__info.copy((char *)mTransmitBuffer, len, 0);
	while(true)
	{
		HAL_StatusTypeDef result=HAL_UART_Transmit(mpUART_HandleTypeDef,mTransmitBuffer,len,1);
		if(result==HAL_StatusTypeDef::HAL_BUSY)
			continue;
		if(result==HAL_StatusTypeDef::HAL_OK)
			mByteCntTransmit+=len;
		return result;
	}
}

void clsUartDma::mWaitUartDmaFree(void) const
{
	while(mpUART_HandleTypeDef->hdmatx->State != HAL_DMA_StateTypeDef::HAL_DMA_STATE_READY){;}
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	clsUartDma::dmaCycleCallBack(huart);
}
