/*
 * myApp.cpp
 *
 *  Created on: Jul 1, 2019
 *      Author: yangh
 */


#include "clsUartDma.h"
#include "usart.h"
#include "myApp.h"
#include <cstdlib>
#include "clsUartLoopTest.h"
#include "clsSlotsOnOff.h"

void myApp(void)
{
	clsUartDma uart1; //=clsUartDma::uartObj(0);
	{
		uart1.init(&huart1);
		uart1.StartReceive();
	}
	UART_HandleTypeDef * uart_s[]={&huart5,nullptr};
	clsUartLoopTest::sInit(uart_s);
//	clsUartDma& uart5=clsUartDma::uartObj(1);
//	{
//		uart5.init(&huart5);
//		uart5.StartReceive();
//	}
//	clsUartLoopTest Uart5LoopTest(uart5);

	uint32_t last_tick=HAL_GetTick();
	uint32_t sec_cnt=0;
	const int ms_tmp=1000;

	GPIO_PinState psa=GPIO_PIN_RESET;
	GPIO_PinState psb=GPIO_PIN_SET;
//	bool Uart5OK=true;
//	clsSlotsOnOff slotsOnOff(6,100);
	while (1)
	{
//		if(Uart5LoopTest.test()==clsUartLoopTest::enumStatus::enumULTSTS_Fail)
//		{
//			Uart5OK=false;
//		}
		{
//			uint32_t slot_x2=slotsOnOff.slot();
//			GPIO_PinState led=GPIO_PIN_RESET;
//			if( (slot_x2&0xfe) ==0)
//			{
//				if(slot_x2&1 )
//					led = GPIO_PIN_SET;
//			}
//			else if( (slot_x2&0xfe) == (1<<1))
//			{
//				if(Uart5OK && (slot_x2&1) )
//					led = GPIO_PIN_SET;
//			}
			HAL_GPIO_WritePin(GPIOB, LED_SYS_Pin, clsUartLoopTest::sTest()?GPIO_PIN_SET:GPIO_PIN_RESET);
		}
	  uint32_t tk=HAL_GetTick();
	  if(tk-last_tick>=ms_tmp)
	  {
		  last_tick+=ms_tmp;
		  sec_cnt++;
		  {
			  GPIO_PinState tmp=psa;
			  psa=psb;
			  psb=tmp;
		  }

		  HAL_GPIO_WritePin(GPIOB, c1_Pin | c3_Pin | c5_Pin | c7_Pin | c9_Pin  , psa);
		  HAL_GPIO_WritePin(GPIOB, c2_Pin | c4_Pin | c6_Pin | c8_Pin | c10_Pin              , psb);

		  {
			  string s;
			  s.append(to_string(sec_cnt));
			  s.append(" ");
			  s.append(1,'0'+psa);
			  s.append(" ");
			  s.append(to_string(clsUartLoopTest::sByteCntGetTotal()));
			  s.append("\r\n");
			  uart1.transmit(s);
		  }
		  {
			  string rece;
			  rece.append("Received: ");
			  int rcnt=0;
			  while(uart1.availableByteCount()>0)
			  {
				  auto c=uart1.getByte();
				  rece.append(1,c);
				  rcnt++;
			  }
			  if(rcnt>0)
			  {
				  rece.append("\r\n");
				  uart1.transmit(rece);
			  }
		  }
//		  uint8_t c='0'+psa;
//
//		  uart1.transmit(&c, 1);
//		  //HAL_UART_Transmit(&huart1, &c, 1,1);

	  }
	  if(last_tick>tk)
		  last_tick=tk;

	}

}

void myApp_old(void)
{
	clsUartDma& uart1=clsUartDma::uartObj(0);
	{
		uart1.init(&huart1);
		uart1.StartReceive();
	}

	clsUartDma& uart5=clsUartDma::uartObj(1);
	{
		uart5.init(&huart5);
		uart5.StartReceive();
	}
	clsUartLoopTest Uart5LoopTest(uart5);

	uint32_t last_tick=HAL_GetTick();
	uint32_t sec_cnt=0;
	const int ms_tmp=1000;

	GPIO_PinState psa=GPIO_PIN_RESET;
	GPIO_PinState psb=GPIO_PIN_SET;
	bool Uart5OK=true;
	clsSlotsOnOff slotsOnOff(6,100);
	while (1)
	{
		if(Uart5LoopTest.test()==clsUartLoopTest::enumStatus::enumULTSTS_Fail)
		{
			Uart5OK=false;
		}
		{
			uint32_t slot_x2=slotsOnOff.slot();
			GPIO_PinState led=GPIO_PIN_RESET;
			if( (slot_x2&0xfe) ==0)
			{
				if(slot_x2&1 )
					led = GPIO_PIN_SET;
			}
			else if( (slot_x2&0xfe) == (1<<1))
			{
				if(Uart5OK && (slot_x2&1) )
					led = GPIO_PIN_SET;
			}
			HAL_GPIO_WritePin(GPIOB, LED_SYS_Pin, led);
		}
	  uint32_t tk=HAL_GetTick();
	  if(tk-last_tick>=ms_tmp)
	  {
		  last_tick+=ms_tmp;
		  sec_cnt++;
		  {
			  GPIO_PinState tmp=psa;
			  psa=psb;
			  psb=tmp;
		  }

		  HAL_GPIO_WritePin(GPIOB, c1_Pin | c3_Pin | c5_Pin | c7_Pin | c9_Pin  , psa);
		  HAL_GPIO_WritePin(GPIOB, c2_Pin | c4_Pin | c6_Pin | c8_Pin | c10_Pin              , psb);

		  {
			  string s;
			  s.append(to_string(sec_cnt));
			  s.append(" ");
			  s.append(1,'0'+psa);
			  s.append(" ");
			  s.append(to_string(uart5.byteCntGet()));
			  s.append("\r\n");
			  uart1.transmit(s);
		  }
		  {
			  string rece;
			  rece.append("Received: ");
			  int rcnt=0;
			  while(uart1.availableByteCount()>0)
			  {
				  auto c=uart1.getByte();
				  rece.append(1,c);
				  rcnt++;
			  }
			  if(rcnt>0)
			  {
				  rece.append("\r\n");
				  uart1.transmit(rece);
			  }
		  }
//		  uint8_t c='0'+psa;
//
//		  uart1.transmit(&c, 1);
//		  //HAL_UART_Transmit(&huart1, &c, 1,1);

	  }
	  if(last_tick>tk)
		  last_tick=tk;

	}

}
