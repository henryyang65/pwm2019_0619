/*
 * clsUartLoopTest.h
 *
 *  Created on: Jul 1, 2019
 *      Author: yangh
 */

#ifndef CLASS_CLSUARTLOOPTEST_H_
#define CLASS_CLSUARTLOOPTEST_H_

#include "clsUartDma.h"
#include "clsSlotsOnOff.h"

class clsUartLoopTest
{
public:
	enum enumStatus
	{
		enumULTSTS_NONE,
		enumULTSTS_Receiving,
		enumULTSTS_Success,
		enumULTSTS_Fail,
	};
private:
	clsUartDma& mUart;
	enumStatus mStatus;
	string mSend;
	string mRece;

	static clsUartLoopTest * pULT_S[clsUartDma::uartClassObjectCount];
	static clsSlotsOnOff smSlotsOnOff;
	static bool smPortOK[clsUartDma::uartClassObjectCount+1];
	static void smInitPortOK(void);
public:
	clsUartLoopTest(clsUartDma& m__uart);
	~clsUartLoopTest();

	enumStatus status(void)const{return mStatus;}
	enumStatus test(void);
	static void sInit(UART_HandleTypeDef * m__puart_s[]);
	static bool sTest(void);
	static uint32_t sByteCntGetTotal(void);
};

#endif /* CLASS_CLSUARTLOOPTEST_H_ */
