/*
 * myApp.h
 *
 *  Created on: Jul 1, 2019
 *      Author: yangh
 */

#ifndef MYAPP_H_
#define MYAPP_H_

#ifdef __cplusplus
extern "C"
{
#endif

void myApp(void);


#ifdef __cplusplus
}
#endif


#endif /* MYAPP_H_ */
