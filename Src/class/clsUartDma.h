/*
 * clsUartDma.h
 *
 *  Created on: Jul 1, 2019
 *      Author: yangh
 */

#ifndef UART_CLSUARTDMA_H_
#define UART_CLSUARTDMA_H_

#include <stdint.h>
#include <stm32f0xx_hal.h>
#include <string>
using namespace std;


class clsUartDma {
public:
	clsUartDma();
	~clsUartDma();

	enum enumStatus
	{
		enumURDS_none,
		enumURDS_Idle,
		enumURDS_Receiving,
		enumURDS_error,
	};
	static const int ReceingBUFFER_SIZE=200;
	static const int Transmit_BUFFER_SIZE=200;
	bool init(UART_HandleTypeDef *m__pUART_HandleTypeDef);
	bool StartReceive(void);
	enumStatus status(void)const{return mStatus;}
	uint32_t availableByteCount(void);
	bool isSameUart(UART_HandleTypeDef *m__pUART_HandleTypeDef)const{return m__pUART_HandleTypeDef == mpUART_HandleTypeDef;}
	uint8_t getByte(void) ;
	int getByteCheck(void);
	HAL_StatusTypeDef transmit(const string & m__info);
	HAL_StatusTypeDef transmit(const uint8_t *m__pa, int m___size);
	HAL_StatusTypeDef transmitDirect(const string & m__info);
	void sInit(void);
	static void dmaCycleCallBack(UART_HandleTypeDef * m__pUart);
	static const int uartClassObjectCount=5;
	static clsUartDma& uartObj(int m__index){return uartClassObjects[m__index];}

	uint32_t  byteCntGet(void)const{return mByteCntGet;}
private:
	UART_HandleTypeDef *mpUART_HandleTypeDef;
	uint32_t mReadIndex;	//to be read index

	//uint32_t mDmaIndex;		//to be write index
	uint32_t mReceByteCnt;
	uint32_t mLastDmaCycle;
	uint32_t mLastCNDTR;

	enumStatus mStatus;
	HAL_StatusTypeDef mHAL_StatusTypeDef;
	uint32_t mDmaCycle;
	uint32_t mReadCycle;
	unsigned char mTransmitBuffer[Transmit_BUFFER_SIZE];
	void mWaitUartDmaFree(void)const;
	unsigned char mBuffer[ReceingBUFFER_SIZE];
	bool mInitBuffer(void);
	void incDmaCycle(UART_HandleTypeDef * m__pUart){if(m__pUart==mpUART_HandleTypeDef) mDmaCycle++;}
	static  clsUartDma  uartClassObjects[uartClassObjectCount];
	uint32_t mErrorCnt;
	uint32_t mByteCntTransmit;
	uint32_t mByteCntReceive;
	uint32_t mByteCntGet;
};

//class clsUartReceiveDma {
//public:
//	enum enumStatus
//	{
//		enumURDS_none,
//		enumURDS_Idle,
//		enumURDS_Receiving,
//		enumURDS_error,
//	};
//	static const int BUFFER_SIZE=200;
//	static const int Transmit_BUFFER_SIZE=200;
//	clsUartReceiveDma();
//	virtual ~clsUartReceiveDma();
//	bool init(UART_HandleTypeDef *m__pUART_HandleTypeDef);
//	bool StartReceive(void);
//	enumStatus status(void)const{return mStatus;}
//	uint32_t availableByteCount(void);
//	bool isSameUart(UART_HandleTypeDef *m__pUART_HandleTypeDef)const{return m__pUART_HandleTypeDef == mpUART_HandleTypeDef;}
//	void incDmaCycle(void){mDmaCycle++;}
//	uint8_t getByte(void) ;
//	HAL_StatusTypeDef transmit(const string & m__info);
//	HAL_StatusTypeDef transmit(const uint8_t *m__pa, int m___size);
//
//private:
//	UART_HandleTypeDef *mpUART_HandleTypeDef;
//	uint32_t mReadIndex;	//to be read index
//	uint32_t mDmaIndex;		//to be write index
//	enumStatus mStatus;
//	HAL_StatusTypeDef mHAL_StatusTypeDef;
//	uint32_t mDmaCycle;
//	uint32_t mReadCycle;
//	unsigned char mTransmitBuffer[Transmit_BUFFER_SIZE];
//	void mWaitUartDmaFree(void)const;
//	unsigned char mBuffer[BUFFER_SIZE];
//	bool mInitBuffer(void);
//};

#endif /* UART_CLSUARTDMA_H_ */
