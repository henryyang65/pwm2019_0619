/*
 * clsUartLoopTest.cpp
 *
 *  Created on: Jul 1, 2019
 *      Author: yangh
 */

#include <clsUartLoopTest.h>
#include <cstdlib>
#include <assert.h>

clsUartLoopTest * clsUartLoopTest::pULT_S[clsUartDma::uartClassObjectCount];
clsSlotsOnOff clsUartLoopTest::smSlotsOnOff(2*clsUartDma::uartClassObjectCount*2,100);
bool clsUartLoopTest::smPortOK[clsUartDma::uartClassObjectCount+1];

clsUartLoopTest::clsUartLoopTest(clsUartDma& m__uart)
	:
	mUart(m__uart),
	mStatus(enumStatus::enumULTSTS_NONE)
{
}

clsUartLoopTest::~clsUartLoopTest()
{
}

clsUartLoopTest::enumStatus clsUartLoopTest::test(void)
{
	switch(mStatus)
	{
		case enumStatus::enumULTSTS_Receiving:
		{
			if(mRece.size()<mSend.size())
			{
				while(mUart.availableByteCount()>0)
				{
					auto c=mUart.getByte();
					mRece.append(1,c);
				}
			}
			else
			{
				if(mRece.compare(mSend)==0)
				{
					mStatus=enumStatus::enumULTSTS_Success;
				}
				else
				{
					mStatus=enumStatus::enumULTSTS_Fail;
				}
				mRece.clear();
			}
			break;
		}
		default:
		{
			int len=rand()%mUart.Transmit_BUFFER_SIZE+1;
			mSend.clear();
			for(int i=0;i<len;i++)
			{
				char c=rand()&0xff;
				mSend.append(1,c);
			}
			if(mSend.size()!=(size_t)len)
				return enumStatus::enumULTSTS_Fail;
			mStatus=enumStatus::enumULTSTS_Receiving;
			mUart.transmit(mSend);
			break;
		}
	}
	return mStatus;
}

void clsUartLoopTest::sInit(UART_HandleTypeDef* m__puart_s[])
{
	if(m__puart_s==nullptr)
		return;
	for(int i=0;i<clsUartDma::uartClassObjectCount;i++)
	{
		UART_HandleTypeDef *puart=*m__puart_s;
		if(puart!=nullptr)
		{
			m__puart_s++;
			clsUartDma &ud=clsUartDma::uartObj(i);
			ud.init(puart);
			ud.StartReceive();
			pULT_S[i]=new clsUartLoopTest(ud);
		}
		else
		{
			pULT_S[i]=nullptr;

		}
	}
	smInitPortOK();
}

void clsUartLoopTest::smInitPortOK(void)
{
	for(int i=0;i<=clsUartDma::uartClassObjectCount;i++)
	{
		smPortOK[i]=true;
	}
}

bool clsUartLoopTest::sTest(void)
{
	for(int i=0;i<clsUartDma::uartClassObjectCount;i++)
	{
		if(pULT_S[i]!=nullptr)
		{
			if(pULT_S[i]->test()==clsUartLoopTest::enumStatus::enumULTSTS_Fail)
			{
				smPortOK[i+1]=false;
			}
		}
		else
		{
				smPortOK[i+1]=false;
		}
	}
	uint32_t slot_x2=smSlotsOnOff.slot();
	if(slot_x2&1)
	{
		slot_x2>>=1;
		if(slot_x2<clsUartDma::uartClassObjectCount+1)
		{

			return smPortOK[slot_x2];
		}
		else
		{
			smInitPortOK();
		}

	}
	return false;

}

uint32_t clsUartLoopTest::sByteCntGetTotal(void)
{
	uint32_t n=0;
	for(int i=0;i<clsUartDma::uartClassObjectCount;i++)
	{
		clsUartDma &ud=clsUartDma::uartObj(i);
		n+=ud.byteCntGet();
	}
	return n;
}
