/*
 * clsSlotsOnOff.h
 *
 *  Created on: Jul. 2, 2019
 *      Author: yangh
 */

#ifndef CLASS_CLSSLOTSONOFF_H_
#define CLASS_CLSSLOTSONOFF_H_
#include <cstdint>

class clsSlotsOnOff
{
private:
	uint32_t mLast_tick;
	uint32_t mSlot_ms_cnt;
	uint32_t mSlot_x2;
public:
	const uint32_t SlotNumber;
	const uint32_t Slot_ms;
	clsSlotsOnOff(uint32_t m__slotNumber,uint32_t m__slot_ms);
	~clsSlotsOnOff();

	uint32_t slot(void);
};

#endif /* CLASS_CLSSLOTSONOFF_H_ */
